from subprocess_shell import *


_v_ = (
    "import sys, time; print('stdout', flush=True); time.sleep(1); print('stderr',"
    " file=sys.stderr)"
)
arguments = ["python", "-c", _v_]


print()
print("default: rich (if available), not ascii")
_ = arguments >> run()
print()
print("not rich, not ascii")
_ = arguments >> run(wait(rich=False, ascii=False))
print()
print("not rich, ascii")
_ = arguments >> run(wait(rich=False, ascii=True))
print()
print("unusual: rich (if available), ascii")
_ = arguments >> run(wait(rich=True, ascii=True))
