from subprocess_shell import *
import typer
import io
import os.path as o_path
import pathlib
import re
import sys
import time


PYTHON_PATH = pathlib.Path(o_path.relpath(sys.executable, "."))


def main(sub: bool = False):
    if sub:
        _v_ = """
Hi!

I'm a sub process and was started by subprocess_shell. In the next couple of seconds I try to convince you that subprocess_shell is worth your time! Lets go :) ...

This is my stdout. You recognize it by the thin border to the left with round corners and green tint if Rich is available.

When I write something to stderr .. like now
"""
        _speak(_v_[1:-1])

        _v_ = """
This is what I wrote when I said now
"""
        print(_v_[1:-1], file=sys.stderr, flush=True)

        _v_ = f"""
 .. you won't see it immediately.
This is a feature, not a bug, and allows you to easily distinguish the output of each process in a chain.

For instance

[{repr(str(PYTHON_PATH))}, "-c", "import sys; print('stdout'); print('stderr', file=sys.stderr)"] >> start() + ["cat", "-"] >> run()

shows

"""
        _speak(_v_[1:-1])

        string_io = io.StringIO()

        _v_ = [
            PYTHON_PATH,
            "-c",
            "import sys; print('stdout'); print('stderr', file=sys.stderr)",
        ] >> start() + ["cat", "-"]
        _ = _v_ >> run(wait(stdout=string_io, stderr=string_io))

        string_io.seek(0)

        _v_ = f"""
{string_io.read()}
The bold border with square corners and little e glyphs represents stderr of the first process, again in orange/red tint if Rich is available.

That's it for now. You can achieve a lot with very little effort, if you decide to try and invest a little time of course!
But you won't regret it, I promise, and if you ever need more control, you can always get to and use the Popen objects directly.

Thank you for your attention and have a great day!

Bye :)
"""
        _speak(_v_[1:-1])

    else:
        _ = [PYTHON_PATH, pathlib.Path(o_path.relpath(__file__, ".")), "--sub"] >> run()


def _speak(string):
    def _print(*args, **kwargs):
        print(*args, end="", flush=True, **kwargs)

    for match in re.finditer(r"[^ ,.!\n]+|[ ,.!\n]", string):
        string = match.group(0)
        if string == " ":
            _print(" ")
            time.sleep(0.2)

        elif string == ",":
            _print(",")
            time.sleep(0.35)

        elif string in "\n.!":
            _print(string)
            time.sleep(0.5)

        else:
            _print(string)


typer.run(main)
